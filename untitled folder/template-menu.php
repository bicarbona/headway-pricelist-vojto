<?php
/*
Template Name: Menu SK
*/

$debut = 0; //The first article to be displayed

function food_menu($in_category, $not_in_category ){
	
	echo '<h5 class="food-menu-title"><span class="title">'.strip_tags(category_description($in_category)).'</span></h5>'."\n";
	echo '<ul class="archive list">'."\n";;

	$args=array(
	'post_type' => 'food_menu',
	//'taxonomy' ='series',
	//'term' => $term->slug,
	'orderby' => 'title',
	'order'=> 'ASC',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	//'post_status' => 'published',
	 'cat' => $in_category,
	
	'category__not_in' => array($not_in_category), 
	);

	$query = null;
	$query = new WP_Query($args);

	while ($query->have_posts()) : $query->the_post(); 

		echo '<li class="item first">'."\n";
			echo '<h4 class="title">'; echo the_title().'</h4>'."\n";
				echo '<ul class="meta">'."\n";
				echo '<li class="remarks">'.get('cena').' &euro;</li>'."\n";
			echo '</ul>'."\n";;
	//	if ( !empty( $post->post_excerpt ) ) : 
	echo '<p class="description">'; echo strip_tags(get_the_excerpt()); echo '</p>'."\n";
	//else : false; endif;
			echo'</li>'."\n";
		endwhile;
	echo'</ul>'."\n";
	echo '<div class="clear"></div>'."\n";
} 
get_header(); ?>
    <div id="content" class="page col-full fix">
		<div id="main" class="fullwidth">
		<?php if ( isset($woo_options[ 'woo_breadcrumbs_show' ]) && $woo_options[ 'woo_breadcrumbs_show' ] == 'true' ) { ?>
			<div id="breadcrumbs">
				<?php woo_breadcrumbs(); ?>
			</div><!--/#breadcrumbs -->
		<?php } ?>
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                <article <?php post_class(); ?>>
				    <h1 class="title"><?php the_title(); ?></h1>
                    <div class="entry">
	                	<?php the_content(); ?>
	               	</div><!-- /.entry -->
					<div class="column one-half food-menu">
						<div class="wrapper">
				<?php
				 if ( current_user_can('manage_options') ) {
					echo '<a href="http://balass/jedalny-listok-print/" title="Print">Print</a>';
					 } 
						// $editor = false;
						if (ICL_LANGUAGE_CODE == 'en') {
							food_menu(46,0); 	// Predjedla
							food_menu(47,0); 	// Polievky
							food_menu(48,0); 	// Kotlikove jedla
							food_menu(50,0); 	// Rybacie speciality
							food_menu(51,0); 	// Tradicne jedla
							food_menu(61,22); 	// Jedla z masa
							food_menu(52,0); 	// Speciality
							food_menu(60,0); 	// Salaty
							food_menu(59,0); 	// Palacinky
							food_menu(58,20); 	// Dezerty
							food_menu(57,0); 	// Prilohy
							food_menu(55,0); 	// Kompoty
							food_menu(56,0); 	// Dresingy
							 //food_menu(12,0); 	// Rybacie speciality
						}if (ICL_LANGUAGE_CODE == 'sk') {
						 	food_menu(9,0); 	// Predjedla
							food_menu(10,0); 	// Polievky
							food_menu(11,0); 	// Kotlikove jedla
							food_menu(12,0); 	// Rybacie speciality
							food_menu(13,0); 	// Tradicne jedla
							food_menu(8,22); 	// Jedla z masa
							food_menu(22,0); 	// Speciality
							food_menu(21,0); 	// Salaty
							food_menu(20,0); 	// Palacinky
							food_menu(5,20); 	// Dezerty
							food_menu(19,0); 	// Prilohy
							food_menu(17,0); 	// Kompoty
							food_menu(18,0); 	// Dresingy
						}
						?>
						</div>
					</div>
					<div class="column one-half last food-menu">
							<div class="wrapper">
						<?php
							if (ICL_LANGUAGE_CODE == 'sk') {

					 		food_menu(24,25);	// Nealkoholicke napoje
					 		food_menu(25,0); 	// Teple napoje
					 		food_menu(26,0); 	// Piva
					 	//	food_menu(33,0); 	// Vodka
					 	//	food_menu(31,0); 	// Gin
					 	//	food_menu(34,0);	// Tequila
					 		food_menu(43,0);	// Tequila & Gin
					 		food_menu(30,0);	// Brandy
					 		food_menu(32,0);	// Whiskey & bourbon 
					 		food_menu(36,0);	// Likery & destilaty 
					 		food_menu(37,0);	// Prava palenka Szicsek 
					 		food_menu(27,0);	// Vina
					 	//	food_menu(28,0);	// Vina
					 		food_menu(38,0);	// Vina biele
					 		food_menu(39,0);	// Vina rose
					 		food_menu(40,0);	// Vina cervene
					 		food_menu(42,0);	// Vina chateau selection
							}
								if (ICL_LANGUAGE_CODE == 'en') {

						 		food_menu(24,25);	// Nealkoholicke napoje
						 		food_menu(25,0); 	// Teple napoje
						 		food_menu(26,0); 	// Piva
						 	//	food_menu(33,0); 	// Vodka
						 	//	food_menu(31,0); 	// Gin
						 	//	food_menu(34,0);	// Tequila
						 		food_menu(43,0);	// Tequila & Gin
						 		food_menu(30,0);	// Brandy
						 		food_menu(32,0);	// Whiskey & bourbon 
						 		food_menu(36,0);	// Likery & destilaty 
						 		food_menu(37,0);	// Prava palenka Szicsek 
						 		food_menu(27,0);	// Vina
						 	//	food_menu(28,0);	// Vina
						 		food_menu(38,0);	// Vina biele
						 		food_menu(39,0);	// Vina rose
						 		food_menu(40,0);	// Vina cervene
						 		food_menu(42,0);	// Vina chateau selection
						 	}
						?>
							</div>
						</div>
					<div class="clear"></div>
				</article><!-- /.post -->
			<?php endwhile; else: ?>
				<article <?php post_class(); ?>>
                	<p><?php _e( 'Sorry, no posts matched your criteria.', 'woothemes' ) ?></p>
                </article><!-- /.post -->
            <?php endif; ?>
		</div><!-- /#main -->
    </div><!-- /#content -->
<?php get_footer(); ?>